# Base images

The Dockerfiles in this directory are used to build the images with all the needed dependencies:

- `plasma_java.Dockerfile`: Debian-based image shipped with maven, openjdk, arrow and plasma-store client binaries
- `plasma_python.Dockerfile`: Debian-based image shipped with miniconda3, pyarrow and plasma-store client binaries
- `plasma_store.Dockerfile`: Debian-based image with plasma-store server binaries

> Those images are already built and can be pulled with the following:
>
> ```
> docker pull donacrio/plasma_store && docker tag donacrio/plasma_store arrow_plasma
> docker pull donacrio/plasma_python && docker tag donacrio/plasma_python arrow_python
> docker pull donacrio/plasma_java && docker tag donacrio/plasma_java arrow_java
> ```
