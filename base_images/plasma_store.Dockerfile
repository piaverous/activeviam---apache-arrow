FROM continuumio/miniconda3

RUN export DEBIAN_FRONTEND=noninteractive && \
  conda install -c conda-forge python=3.6 arrow-cpp=0.16 -y

ENTRYPOINT ["/bin/bash"]