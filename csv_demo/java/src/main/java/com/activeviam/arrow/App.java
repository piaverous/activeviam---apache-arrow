package com.activeviam.arrow;

import com.activeviam.arrow.exceptions.EnvVariableMalformedException;
import com.activeviam.arrow.exceptions.EnvVariableNotSetException;
import com.activeviam.arrow.exceptions.NoFileTimeoutException;
import com.activeviam.arrow.exceptions.SystemEnvAccessNotAllowedException;
import com.activeviam.arrow.io.ReadFromCsv;
import com.activeviam.arrow.utils.EnvUtils;
import com.activeviam.arrow.utils.FileUtils;
import com.activeviam.arrow.utils.Timer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App implements Runnable {
  private static final Logger logger = LoggerFactory.getLogger(App.class);

  private final int serverSocketPort;
  private final Timer timer;
  private final int maxLoops;
  private final String dataRoot;
  private int numberOfLoops;
  private boolean isRunning;

  public App() throws EnvVariableNotSetException, SystemEnvAccessNotAllowedException,
      EnvVariableMalformedException, IOException {
    this.serverSocketPort = EnvUtils.getEnvVariableAsInteger("SERVER_SOCKET_PORT");
    this.timer = new Timer(true);
    this.maxLoops = EnvUtils.getEnvVariableAsInteger("LOOPS");
    this.isRunning = false;
    this.dataRoot = EnvUtils.getEnvVariableAsString("DATA_ROOT");
    this.numberOfLoops = 1;
  }

  @Override
  public void run() {
    logger.info("Starting the Java process");
    this.isRunning = true;
    this.timer.start();
    ServerSocket serverSocket = null;
    Socket clientSocket = null;
    BufferedWriter bufOutputWriter;
    ReadFromCsv readFromCsv;

    try {
      readFromCsv = new ReadFromCsv(this.timer);
      serverSocket = new ServerSocket(this.serverSocketPort);

      while (this.numberOfLoops <= this.maxLoops && this.isRunning) {
        int numberOfRows = (int) Math.pow(10, this.numberOfLoops);
        String filename = String.format("%s/example.%s.arrow", this.dataRoot, numberOfRows);
        this.timer.debug("Awaiting client connection");
        logger.debug("Socket server listening on {port={}}", this.serverSocketPort);
        clientSocket = serverSocket.accept();
        this.timer.info("Client connected to socket");

        // We use a BufferedInputStream to increase reading performances
        bufOutputWriter =
            new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        bufOutputWriter.write("STA");
        bufOutputWriter.flush();
        this.timer.debug("Sent 'STA' (start) signal to Python client");

        try {
          timer.debug("Waiting for file to be present");
          FileUtils.waitForFile(filename);
          timer.debug(String.format("Found file '%s'", filename));

          File csvFile = FileUtils.validateFile(filename, true);
          FileReader csvFileReader = new FileReader(csvFile);
          readFromCsv.read(csvFileReader);
          this.timer.info(String.format("Done reading the Arrow Table, %d rows", numberOfRows));
        } catch (InterruptedException | NoFileTimeoutException e) {
          this.timer.info("An error occurred while reading the Arrow Table");
          e.printStackTrace();
        } finally {
          this.numberOfLoops++;
        }
      }

    } catch (IOException ex) {
      logger.error("An error occurred", ex);
    } finally {
      this.timer.stop();
      this.stop();
      try {
        logger.info("Closing streams and sockets");
        this.isRunning = false;
        if (!Objects.isNull(clientSocket)) {
          clientSocket.close();
        }
        if (!Objects.isNull(serverSocket)) {
          serverSocket.close();
        }
      } catch (IOException ex) {
        logger.error("An error occurred", ex);
      }
      this.stop();
    }
    logger.info("Java process terminated");
  }

  public void stop() {
    this.isRunning = false;
  }
}
