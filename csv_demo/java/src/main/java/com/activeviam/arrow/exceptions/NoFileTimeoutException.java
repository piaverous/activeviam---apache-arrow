package com.activeviam.arrow.exceptions;

public class NoFileTimeoutException extends Throwable {
  public NoFileTimeoutException(String filePath) {
    super("Max retries exceeded in looking for file '" + filePath + "'");
  }
}
