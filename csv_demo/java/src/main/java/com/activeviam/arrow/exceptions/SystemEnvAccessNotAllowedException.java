package com.activeviam.arrow.exceptions;

public class SystemEnvAccessNotAllowedException extends Throwable {
  public SystemEnvAccessNotAllowedException() {
    super("Security policy doesn't allow access to system environment");
  }
}
