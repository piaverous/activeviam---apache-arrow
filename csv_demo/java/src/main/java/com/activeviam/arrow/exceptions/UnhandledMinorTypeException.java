package com.activeviam.arrow.exceptions;

import org.apache.arrow.vector.types.Types;

public class UnhandledMinorTypeException extends Throwable {
    public UnhandledMinorTypeException(Types.MinorType minorType) {
        super("Unhandled Apache Arrow minor type: " + minorType);
    }
}
