package com.activeviam.arrow.io;

import com.activeviam.arrow.utils.NumUtils;
import com.activeviam.arrow.utils.ParseResult;
import com.activeviam.arrow.utils.Timer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadFromCsv {
  private static final Logger logger = LoggerFactory.getLogger(ReadFromCsv.class);

  private final Timer timer;
  private final int maxNumberOfLogs;

  public long nullEntries;
  private long totalChecksum;
  private long intChecksum;
  private long longChecksum;
  private long doubleChecksum;
  private long floatChecksum;
  private long stringChecksum;

  public ReadFromCsv(Timer parentTimer) {
    this.timer = parentTimer;
    this.maxNumberOfLogs = 10;
    this.nullEntries = 0;
    this.totalChecksum = 0;
    this.intChecksum = 0;
    this.longChecksum = 0;
    this.doubleChecksum = 0;
    this.floatChecksum = 0;
    this.stringChecksum = 0;
  }

  public void read(FileReader fileReader) throws IOException {
    BufferedReader csvReader = new BufferedReader(fileReader);
    String[] firstRow = csvReader.readLine().split(",");
    logger.debug("Schema is {}", Arrays.toString(firstRow));

    int i = 0;
    String row;
    this.timer.debug("Starting file read");
    while ((row = csvReader.readLine()) != null) {
      String[] rowContent = row.split(",");
      boolean displayLogs = i < this.maxNumberOfLogs;
      if (displayLogs) {
        logger.debug("\tRow {}", i);
      }
      for (int j = 0; j < firstRow.length; j++) {
        this.handleDataEntry(rowContent[j], displayLogs, firstRow[j]);
      }
      i++;
    }
    logger.debug("Nothing more in stream");
    fileReader.close();
    long currentCheckSum =
        this.intChecksum + this.longChecksum + this.doubleChecksum + this.floatChecksum +
            this.stringChecksum;
    logger.debug("intCheckSum {} longCheckSum {} doubleCheckSum {} floatSum {} stringChecksum {} = "
            + "{}", this.intChecksum, this.longChecksum, this.doubleChecksum, this.floatChecksum,
        currentCheckSum, stringChecksum);
    if (this.totalChecksum != currentCheckSum) {
      logger.warn("Column Checksum mismatch > {} != {} / difference : {}", this.totalChecksum,
          currentCheckSum, this.totalChecksum - currentCheckSum);
    }
  }

  public void handleDataEntry(String string, boolean displayLogs, String columnName) {
    ParseResult<?> parseResult;
    if ((parseResult = NumUtils.isInteger(string)).success) {
      this.showIntAccessor((ParseResult<Integer>) parseResult, displayLogs, columnName);
    } else if ((parseResult = NumUtils.isLong(string)).success) {
      this.showBigIntAccessor((ParseResult<Long>) parseResult, displayLogs, columnName);
    } else if ((parseResult = NumUtils.isDouble(string)).success) {
      this.showDoubleAccessor((ParseResult<Double>) parseResult, displayLogs, columnName);
    } else if ((parseResult = NumUtils.isFloat(string)).success) {
      this.showFloatAccessor((ParseResult<Float>) parseResult, displayLogs, columnName);
    } else {
      this.showStringAccessor(new ParseResult<>(true, string), displayLogs, columnName);
    }
  }

  private void showIntAccessor(ParseResult<Integer> parseResult, boolean display,
                               String columnName) {
    int value = parseResult.result;
    if (display) {
      logger.debug("\t\t {} - {} (int)", columnName, value);
    }
    this.intChecksum += value;
    this.totalChecksum += value;
  }

  private void showBigIntAccessor(ParseResult<Long> parseResult, boolean display,
                                  String columnName) {
    long value = parseResult.result;
    if (display) {
      logger.debug("\t\t {} - {} (long)", columnName, value);
    }
    this.longChecksum += value;
    this.totalChecksum += value;
  }

  private void showFloatAccessor(ParseResult<Float> parseResult, boolean display,
                                 String columnName) {
    float value = parseResult.result;
    if (display) {
      logger.debug("\t\t {} - {} (float)", columnName, value);
    }
    this.floatChecksum += (long) value;
    this.totalChecksum += (long) value;
  }

  private void showDoubleAccessor(ParseResult<Double> parseResult, boolean display,
                                  String columnName) {
    double value = parseResult.result;
    if (display) {
      logger.debug("\t\t {} - {} (double)", columnName, value);
    }
    this.doubleChecksum += (long) value;
    this.totalChecksum += (long) value;
  }

  private void showStringAccessor(ParseResult<String> parseResult, boolean display,
                                  String columnName) {
    String value = parseResult.result;
    if (display) {
      logger.debug("\t\t {} - {} (string)", columnName, value);
    }
    this.stringChecksum += value.length();
    this.totalChecksum += value.length();
  }
}