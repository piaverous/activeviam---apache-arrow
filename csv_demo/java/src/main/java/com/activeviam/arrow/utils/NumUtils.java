package com.activeviam.arrow.utils;

public class NumUtils {
  public static ParseResult<Integer> isInteger(String s) {
    try {
      return new ParseResult<>(true, Integer.parseInt(s));
    } catch (NumberFormatException e) {
      return new ParseResult<>(false, 0);
    }
  }

  public static ParseResult<Double> isDouble(String s) {
    try {
      return new ParseResult<>(true, Double.parseDouble(s));
    } catch (NumberFormatException e) {
      return new ParseResult<>(false, 0d);
    }
  }

  public static ParseResult<Long> isLong(String s) {
    try {
      return new ParseResult<>(true, Long.parseLong(s));
    } catch (NumberFormatException e) {
      return new ParseResult<>(false, 0l);
    }
  }

  public static ParseResult<Float> isFloat(String s) {
    try {
      return new ParseResult<>(true, Float.parseFloat(s));
    } catch (NumberFormatException e) {
      return new ParseResult<>(false, 0f);
    }
  }

}
