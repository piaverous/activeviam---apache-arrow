package com.activeviam.arrow.utils;

public class ParseResult<T> {
  public boolean success;
  public T result;

  public ParseResult(boolean success, T result) {
    this.success = success;
    this.result = result;
  }
}
