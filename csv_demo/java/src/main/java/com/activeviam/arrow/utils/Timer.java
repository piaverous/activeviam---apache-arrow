package com.activeviam.arrow.utils;

import com.activeviam.arrow.exceptions.EnvVariableNotSetException;
import com.activeviam.arrow.exceptions.SystemEnvAccessNotAllowedException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Timer {
  private static final Logger logger = LoggerFactory.getLogger(Timer.class);

  private final StopWatch watch;
  private final List<Long> laps;
  private final boolean logToFile;
  private long startTime;
  private long stopTime;
  private PrintWriter logWriter;

  public Timer(boolean logToFile)
      throws EnvVariableNotSetException, SystemEnvAccessNotAllowedException, IOException {
    this.watch = new StopWatch();
    this.laps = new ArrayList<>();
    this.logToFile = logToFile;
    if (this.logToFile) {
      String logFilePath = String
          .format("%s/JAVA_%s.log", EnvUtils.getEnvVariableAsString("LOG_PATH"),
              System.currentTimeMillis());
      this.logWriter = new PrintWriter(new FileWriter(logFilePath));
    }
  }

  public void start() {
    this.watch.start();
    this.startTime = this.watch.getTime();
    this.laps.add(this.startTime);
  }

  public void debug(String msg) {
    this.lap(msg);
    logger.debug(msg);
  }

  public void info(String msg) {
    this.lap(msg);
    logger.info(msg);
  }

  public void stop() {
    this.watch.stop();
    this.stopTime = this.watch.getTime();
    this.laps.add(this.stopTime);
    String msg = String.format("Total - %sms", this.totalDuration());
    this.writeLog(msg);
    logger.debug(msg);
    this.close();
  }

  private void lap(@Nullable String msg) {
    msg = Optional.ofNullable(msg).orElse("Lap");
    this.laps.add(this.watch.getTime());
    this.writeLog(String.format("%s - %sms", msg, this.latestLapDuration()));
  }

  private void writeLog(String msg) {
    if (this.logToFile) {
      this.logWriter.printf("[%s] - %s\n", Timer.class, msg);
      this.logWriter.flush();
    }
  }

  private void close() {
    if (this.logToFile) {
      this.logWriter.close();
    }
  }

  private long latestLapDuration() {
    int len = this.laps.size();
    if (len >= 2) {
      return this.laps.get(len - 1) - this.laps.get(len - 2);
    }
    return 0;
  }

  private long totalDuration() {
    return this.stopTime - this.startTime;
  }
}
