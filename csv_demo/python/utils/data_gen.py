import os

import numpy as np
import pandas as pd


def gen_random_string_list(n_rows, word_length=10):
    min_lc = ord(b'a')
    len_lc = 26
    ba = bytearray(os.urandom(n_rows * word_length))
    for i, b in enumerate(ba):
        ba[i] = min_lc + b % len_lc  # convert 0..255 to 97..122
    super_long_word = ba.decode("utf-8")
    return list(map(''.join, zip(*[iter(super_long_word)] * word_length)))


def gen_dataframe(n_rows=100):
    int_list = np.random.randint(1024, size=n_rows, dtype=np.int32)
    long_list = np.random.randint(1e10, size=n_rows, dtype=np.int64)
    float_list = np.random.rand(n_rows).astype(np.float32)
    double_list = np.random.randn(n_rows)
    string_list = gen_random_string_list(n_rows, 12)
    bin_list = list(map(lambda txt: txt.encode(), string_list))

    data = {
        'int': int_list,
        'long': long_list,
        'binary': bin_list,
        'float': float_list,
        'double': double_list,
        'string': string_list
    }

    df = pd.DataFrame(data)
    return df
