def expect_start_signal(socket):
    start_signal = socket.recv(8)
    if start_signal != b"STA":
        raise Exception("Signal does not correspond to start signal")
    return start_signal


import pyarrow as pa


def write_arrow_table(table, filename):
    """Writes an 1rrow Table to a file

    Arguments:
        table {pa.Table} -- Data as an Arrow Table
        filename {string} -- Path to the file to write to
    """
    with open(filename, 'wb') as f:
        writer = pa.RecordBatchFileWriter(f, table.schema)
        writer.write(table)
        writer.close()


def read_arrow_table(filename):
    """Reads an 1rrow Table from a file

    Arguments:
        filename {string} -- Path to the file to read

    Returns:
        [pa.Table] -- The contents of the file, as an Arrow Table
    """
    table = None
    with open(filename, 'rb') as f:
        reader = pa.RecordBatchFileReader(f)
        table = reader.read_all()
    return table
