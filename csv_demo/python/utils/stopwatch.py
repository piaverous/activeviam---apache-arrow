import timeit
from typing import List

from utils.docker_logger import DockerLogger


class StopWatch:
    """Measure time used."""

    def __init__(self, log_level, log_to_stdout=False, log_to_file=True):
        self._laps = []
        self.logger = DockerLogger("stopwatch", log_level=log_level, log_to_stdout=log_to_stdout,
                                   log_to_file=log_to_file)

        self._start_time = 0
        self._stop_time = 0

    def start(self) -> float:
        self._start_time = timeit.default_timer()
        self._laps.append(self._start_time)
        return self._start_time

    def lap(self, msg="Lap") -> List[float]:
        self._laps.append(timeit.default_timer())
        self.logger.info(f"{msg} - {self.latest_lap_duration()}ms")
        return self._laps

    def stop(self, msg="Total") -> float:
        self._stop_time = timeit.default_timer()
        self._laps.append(self._stop_time)
        self.logger.info(f"{msg} - {self}ms")
        return self._stop_time

    def latest_lap_duration(self) -> str:
        if len(self._laps) >= 2:
            millis = (self._laps[-1] - self._laps[-2]) * 1000
            return f"{millis:.2f}"
        else:
            return "0"

    def __str__(self) -> str:
        millis = (self._stop_time - self._start_time) * 1000
        return f"{millis:.2f}"
