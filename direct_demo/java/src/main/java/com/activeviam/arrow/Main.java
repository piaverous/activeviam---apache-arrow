package com.activeviam.arrow;

import com.activeviam.arrow.exceptions.EnvVariableMalformedException;
import com.activeviam.arrow.exceptions.EnvVariableNotSetException;
import com.activeviam.arrow.exceptions.SystemEnvAccessNotAllowedException;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {
    try {
      App app = new App();

      Runtime.getRuntime()
          .addShutdownHook(
              new Thread(
                  () -> {
                    app.stop();
                    System.exit(0);
                  }));

      app.run();
    } catch (EnvVariableNotSetException
        | SystemEnvAccessNotAllowedException
        | EnvVariableMalformedException
        | IOException ex) {
      logger.error("A system error occurred", ex);
      System.exit(1);
    }
    System.exit(0);
  }
}
