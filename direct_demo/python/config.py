import os

SERVER_SOCKET_HOST = os.environ.get("SERVER_SOCKET_HOST")
SERVER_SOCKET_PORT = os.environ.get("SERVER_SOCKET_PORT")
LOOPS = os.environ.get("LOOPS")

LOG_LEVEL = os.environ.get("LOGGING_LEVEL")
LOG_PATH = os.environ.get("LOG_PATH")

GLOBAL_TIMEOUT = 300  # in seconds
