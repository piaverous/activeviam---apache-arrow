#!/usr/bin/env python3
import socket

import pyarrow as pa

from config import SERVER_SOCKET_HOST, SERVER_SOCKET_PORT, LOG_LEVEL, GLOBAL_TIMEOUT, LOOPS
from utils.data_gen import gen_dataframe
from utils.docker_logger import DockerLogger
from utils.env_utils import get_log_level_as_int
from utils.io import expect_start_signal, send_table_as_one, send_table_record_batch_wise
from utils.looper import Looper
from utils.stopwatch import StopWatch
from utils.string_utils import human_readable_size

LOGGER = DockerLogger("main", log_level=get_log_level_as_int(LOG_LEVEL))
DEMO_LOGGER = DockerLogger("demo", log_level=get_log_level_as_int(LOG_LEVEL))
STOP_WATCH = StopWatch(log_level=0)
SEND_WHOLE_TABLE = True


def run_the_demo(n_rows=1e6):
    DEMO_LOGGER.info(f"Started demo with n_rows={n_rows}")
    STOP_WATCH.lap(f"Started demo with n_rows={n_rows}")

    DEMO_LOGGER.debug("Generating dataframe")
    df = gen_dataframe(n_rows)
    df_mem_size_readable = human_readable_size(df.memory_usage(index=True).sum())
    STOP_WATCH.lap(f"Generated dataframe, rows: {n_rows}, size: {df_mem_size_readable}")

    DEMO_LOGGER.debug("Converting dataframe to Arrow Table")
    table = pa.Table.from_pandas(df)
    STOP_WATCH.lap("Converted dataframe to Arrow Table")

    DEMO_LOGGER.debug("Connecting to java server socket")
    client_socket = socket.socket()
    client_socket.connect((SERVER_SOCKET_HOST, int(SERVER_SOCKET_PORT)))
    STOP_WATCH.lap("Connected to java server socket")

    DEMO_LOGGER.debug("Waiting for 'STA' signal")
    start_signal = expect_start_signal(client_socket)
    STOP_WATCH.lap("Got 'STA' signal")

    if SEND_WHOLE_TABLE:
        DEMO_LOGGER.debug(f"Streaming whole {n_rows} row Arrow Table through socket.")
        send_table_as_one(table, client_socket)
        STOP_WATCH.lap("Sent whole Arrow Table")
    else:
        DEMO_LOGGER.debug(f"Streaming {n_rows} row Arrow Table one RecordBatch at a time.")
        send_table_record_batch_wise(table, client_socket)
        STOP_WATCH.lap("Sent Arrow Table one RecordBatch at a time")

    DEMO_LOGGER.debug("Closing socket connection")
    client_socket.close()
    del df
    del table
    STOP_WATCH.lap("Closed socket connection")

    return start_signal == b"STA"


if __name__ == "__main__":
    LOGGER.info("Starting the Python process")
    n_iterations = int(LOOPS)

    LOGGER.info(f"Launching a looper to run the demo {n_iterations} times")
    STOP_WATCH.start()
    looper = Looper(run_the_demo, log_level=get_log_level_as_int(LOG_LEVEL), loop_limit=n_iterations)
    looper.start()
    LOGGER.info("Looper started")

    looper.join(GLOBAL_TIMEOUT)
    if looper.is_alive():
        looper.stop()
    STOP_WATCH.stop()

    LOGGER.info("Python process terminated")
