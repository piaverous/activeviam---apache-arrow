def get_log_level_as_int(log_level_as_string):
    if log_level_as_string == "debug":
        return 0
    elif log_level_as_string == "info":
        return 1
    elif log_level_as_string == "warn":
        return 2
    elif log_level_as_string == "error":
        return 3
    else:
        return 0
