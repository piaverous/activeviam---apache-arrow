import pyarrow as pa


def expect_start_signal(socket):
    start_signal = socket.recv(8)
    if start_signal != b"STA":
        raise Exception("Signal does not correspond to start signal")
    return start_signal


def send_table_as_one(table, socket):
    """Sends an Arrow Table through a socket, all at once.
    
    Arguments:
        table {pa.table} -- The Arrow Table to send through
        socket {socket} -- The socket
    
    Performance (ran 'time docker-compose up"):
        SIZE 10 - docker-compose up  0,43s user 0,08s system 17% cpu 2,833 total
        SIZE 1M - docker-compose up  0,47s user 0,10s system 1% cpu 28,463 total
    """
    socket_sink = socket.makefile('wb')
    writer = pa.RecordBatchStreamWriter(socket_sink, table.schema)
    writer.write_table(table)
    writer.close()


def send_table_record_batch_wise(table, socket):
    """Sends an Arrow Table through a socket, a record batch at a time.
    
    Arguments:
        table {pa.table} -- The Arrow Table to send through
        socket {socket} -- The socket
    
    Performance (ran 'time docker-componse up"):
        SIZE 10 - docker-compose up  0,42s user 0,07s system 17% cpu 2,805 total
        SIZE 1M - docker-compose up  0,46s user 0,13s system 2% cpu 28,147 total
    """
    batches = table.to_batches()
    assert len(batches) > 0

    socket_sink = socket.makefile('wb')
    writer = pa.RecordBatchStreamWriter(socket_sink, batches[0].schema)
    for batch in batches:
        writer.write_batch(batch)
    writer.close()
