# Dump Demo

To run this demo, just do the following:

```
docker-compose up
```

> ⚠ This could take some time if it's the first time you're running the project.

## Logs

Logs are located in the `logs` folder. They are named after the process that spawned them. 
`.flog` files are committed as "final logs" onto the repository for further sharing.