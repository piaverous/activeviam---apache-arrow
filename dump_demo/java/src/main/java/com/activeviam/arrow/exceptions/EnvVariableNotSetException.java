package com.activeviam.arrow.exceptions;

public class EnvVariableNotSetException extends Throwable {
  public EnvVariableNotSetException(String envVariableName) {
    super(String.format("Environment variable %s is not set", envVariableName));
  }
}
