package com.activeviam.arrow.io;

import com.activeviam.arrow.exceptions.UnhandledMinorTypeException;
import com.activeviam.arrow.utils.Timer;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.apache.arrow.vector.BigIntVector;
import org.apache.arrow.vector.FieldVector;
import org.apache.arrow.vector.Float4Vector;
import org.apache.arrow.vector.Float8Vector;
import org.apache.arrow.vector.IntVector;
import org.apache.arrow.vector.VarBinaryVector;
import org.apache.arrow.vector.VarCharVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.ipc.ArrowReader;
import org.apache.arrow.vector.types.Types;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadFromArrowStream {
  private static final Logger logger = LoggerFactory.getLogger(ReadFromArrowStream.class);

  private final Timer timer;
  private final int maxNumberOfLogs;

  public long nullEntries;
  private long totalChecksum;
  private long intChecksum;
  private long longChecksum;
  private long arrChecksum;
  private long floatChecksum;
  private long varcharChecksum;

  public ReadFromArrowStream(Timer parentTimer) {
    this.timer = parentTimer;
    this.maxNumberOfLogs = 10;
    this.nullEntries = 0;
    this.totalChecksum = 0;
    this.intChecksum = 0;
    this.longChecksum = 0;
    this.arrChecksum = 0;
    this.floatChecksum = 0;
    this.varcharChecksum = 0;
  }

  public void read(ArrowReader arrowReader) throws IOException, UnhandledMinorTypeException {
    VectorSchemaRoot root = arrowReader.getVectorSchemaRoot();
    logger.debug("Schema is {}", root.getSchema().toString());

    int i = 0;
    while (arrowReader.loadNextBatch()) {
      this.timer.debug(String.format("Loaded batch, %d rows", root.getRowCount()));
      /* we can now process this batch, it is now loaded */
      logger.debug("\t[{}] row count for this block is {}", i, root.getRowCount());
      List<FieldVector> fieldVector = root.getFieldVectors();
      logger.debug(String.format("\t[%d] number of fieldVectors (corresponding to columns) : %d",
        i, fieldVector.size()));
      for (FieldVector valueVectors : fieldVector) {
        Types.MinorType mt = valueVectors.getMinorType();
        switch (mt) {
          case INT:
            showIntAccessor(valueVectors);
            break;
          case BIGINT:
            showBigIntAccessor(valueVectors);
            break;
          case VARBINARY:
            showVarBinaryAccessor(valueVectors);
            break;
          case FLOAT4:
            showFloat4Accessor(valueVectors);
            break;
          case FLOAT8:
            showFloat8Accessor(valueVectors);
            break;
          case VARCHAR:
            showVarcharAccessor(valueVectors);
            break;
          default:
            throw new UnhandledMinorTypeException(mt);
        }
      }
      i++;
    }
    logger.debug("Nothing more in stream");
    arrowReader.close();
    long currentCheckSum =
      this.intChecksum + this.longChecksum + this.arrChecksum + this.floatChecksum + this.varcharChecksum;
    logger.debug("intCheckSum {} longCheckSum {} arrCheckSum {} floatSum {} varcharChecksum {} = "
      + "{}", this.intChecksum, this.longChecksum, this.arrChecksum, this.floatChecksum,
      currentCheckSum, varcharChecksum);
    if (this.totalChecksum != currentCheckSum) {
      logger.warn("Column Checksum mismatch > {} != {} / difference : {}", this.totalChecksum,
        currentCheckSum, this.totalChecksum - currentCheckSum);
    }
  }

  private void showIntAccessor(FieldVector fx) {
    IntVector intVector = ((IntVector) fx);
    for (int j = 0; j < intVector.getValueCount(); j++) {
      if (!intVector.isNull(j)) {
        int value = intVector.get(j);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t intAccessor[{}] {}", j, value);
        }
        this.intChecksum += value;
        this.totalChecksum += value;
      } else {
        this.nullEntries++;
      }
    }
  }

  private void showBigIntAccessor(FieldVector fx) {
    BigIntVector bigIntVector = ((BigIntVector) fx);
    for (int j = 0; j < bigIntVector.getValueCount(); j++) {
      if (!bigIntVector.isNull(j)) {
        long value = bigIntVector.get(j);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t bigIntAccessor[{}] {}", j, value);
        }
        this.longChecksum += value;
        this.totalChecksum += value;
      } else {
        this.nullEntries++;
      }
    }
  }

  private void showVarBinaryAccessor(FieldVector fx) {
    VarBinaryVector varBinaryVector = ((VarBinaryVector) fx);
    for (int j = 0; j < varBinaryVector.getValueCount(); j++) {
      if (!varBinaryVector.isNull(j)) {
        byte[] value = varBinaryVector.get(j);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t varBinaryAccessor[{}] {}", j, Arrays.toString(value));
        }
        this.arrChecksum += value.length;
        this.totalChecksum += value.length;
      } else {
        this.nullEntries++;
      }
    }
  }

  private void showFloat4Accessor(FieldVector fx) {
    Float4Vector float4Vector = ((Float4Vector) fx);
    for (int j = 0; j < float4Vector.getValueCount(); j++) {
      if (!float4Vector.isNull(j)) {
        float value = float4Vector.get(j);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t float4[{}] {}", j, value);
        }
        this.floatChecksum += (long) value;
        this.totalChecksum += (long) value;
      } else {
        this.nullEntries++;
      }
    }
  }

  private void showFloat8Accessor(FieldVector fx) {
    Float8Vector float8Vector = ((Float8Vector) fx);
    for (int j = 0; j < float8Vector.getValueCount(); j++) {
      if (!float8Vector.isNull(j)) {
        double value = float8Vector.get(j);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t float8[{}] {}", j, value);
        }
        this.floatChecksum += (long) value;
        this.totalChecksum += (long) value;
      } else {
        this.nullEntries++;
      }
    }
  }

  private void showVarcharAccessor(FieldVector fx) {
    VarCharVector varcharVector = ((VarCharVector) fx);
    for (int j = 0; j < varcharVector.getValueCount(); j++) {
      if (!varcharVector.isNull(j)) {
        String value = new String(varcharVector.get(j), StandardCharsets.UTF_8);
        if (j <= this.maxNumberOfLogs) {
          logger.debug("\t\t varchar[{}] {}", j, value);
        }
        this.varcharChecksum += value.length();
        this.totalChecksum += value.length();
      } else {
        this.nullEntries++;
      }
    }
  }
}
