package com.activeviam.arrow.utils;

import com.activeviam.arrow.exceptions.EnvVariableMalformedException;
import com.activeviam.arrow.exceptions.EnvVariableNotSetException;
import com.activeviam.arrow.exceptions.SystemEnvAccessNotAllowedException;
import java.util.Objects;

public class EnvUtils {
  public static String getEnvVariableAsString(String key)
      throws EnvVariableNotSetException, SystemEnvAccessNotAllowedException {
    try {
      String value = System.getenv(key);
      if (Objects.isNull(value)) {
        throw new EnvVariableNotSetException(key);
      }
      return value;
    } catch (SecurityException ex) {
      throw new SystemEnvAccessNotAllowedException();
    }
  }

  public static Integer getEnvVariableAsInteger(String key)
      throws EnvVariableNotSetException, SystemEnvAccessNotAllowedException,
             EnvVariableMalformedException {
    try {
      return Integer.valueOf(getEnvVariableAsString(key));
    } catch (NumberFormatException ex) {
      throw new EnvVariableMalformedException(key, "Cannot be parsed as an integer");
    }
  }
}
