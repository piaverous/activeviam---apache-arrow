package com.activeviam.arrow.utils;

import com.activeviam.arrow.exceptions.NoFileTimeoutException;
import java.io.File;

public class FileUtils {
  public static void waitForFile(String filePath)
      throws NoFileTimeoutException, InterruptedException {
    int maxRetries = 120;
    boolean exists = false;

    File file = new File(filePath);
    while (!exists && maxRetries > 0) {
      exists = file.exists() && file.isFile();
      maxRetries -= 1;
      Thread.sleep(1000);
    }
    if (!exists) {
      throw new NoFileTimeoutException(filePath);
    }
  }

  public static File validateFile(String fileName, boolean shouldExist) {
    if (fileName == null) {
      throw new IllegalArgumentException("missing file parameter");
    }
    File f = new File(fileName);
    if (shouldExist && (!f.exists() || f.isDirectory())) {
      throw new IllegalArgumentException(fileName + " file not found: " + f.getAbsolutePath());
    }
    if (!shouldExist && f.exists()) {
      throw new IllegalArgumentException(fileName + " file already exists: " + f.getAbsolutePath());
    }
    return f;
  }
}
