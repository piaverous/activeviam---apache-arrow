from datetime import datetime

from config import LOG_PATH

# current date and time
LOG_FILENAME = f"{LOG_PATH}/PYTHON_{datetime.now().timestamp()}.log"


class DockerLogger:
    """A logger class that uses only the basic print. 
    
    Needed because classic 'logging' module does not output anything in docker logs.
    """

    def __init__(self, name, log_level, log_to_stdout=True, log_to_file=False):
        """Constructor.
        
        Arguments:
            name {string} -- Display name of the logger
            log_level {int} -- 0=DEBUG, 1=INFO, 2=WARN, 3=ERROR
        """
        self.name = name.strip().upper()
        self.log_level = log_level
        self.log_to_stdout = log_to_stdout
        self.log_to_file = log_to_file
        if log_to_file:
            self.log_file = open(LOG_FILENAME, "w")

    def log(self, msg):
        """Basic logger function. All rely on this
        
        Arguments:
            msg {string} -- message to display
        """
        message = f"[{self.name}] - {msg}"
        if self.log_to_stdout:
            print(message)
        if self.log_to_file and self.log_file:
            self.log_file.write(message + '\n')
            self.log_file.flush()

    def debug(self, msg):
        """Debug level logs
        
        Arguments:
            msg {string} -- message to display
        """
        if self.log_level == 0:
            self.log(f"DEBUG - {msg}")

    def info(self, msg):
        """Info level logs
        
        Arguments:
            msg {string} -- message to display
        """
        if self.log_level <= 1:
            self.log(f"INFO - {msg}")

    def warn(self, msg):
        """Warning level logs
        
        Arguments:
            msg {string} -- message to display
        """
        if self.log_level <= 2:
            self.log(f"WARN - {msg}")

    def err(self, msg):
        """Error level logs
        
        Arguments:
            msg {string} -- message to display
        """
        if self.log_level <= 3:
            self.log(f"ERR - {msg}")

    def __del__(self):
        """Destructor. Close open log file if still there and open.
        """
        if self.log_to_file and self.log_file:
            self.log_file.close()
