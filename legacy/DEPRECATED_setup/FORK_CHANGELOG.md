# Changelog for Fork

## Link to the fork repo
https://github.com/piaverous/arrow

## Log

There are some issues on master branch so we need to manually make some changes. The flatbuffers lib is not used anymore but it is still required for building the binaries. We need to remove it. \
If this issue [https://issues.apache.org/jira/browse/ARROW-6799] is still unresolved you need to update `arrow/cpp/src/plasma/CMakeLists.txt` as follow:
- line 193 replace:
```
target_link_libraries(plasma_java
                          plasma_shared
                          ${PLASMA_LINK_LIBS}
                          "-undefined dynamic_lookup"
                          -Wl,-force_load,flatbuffers::flatbuffers
                          flatbuffers::flatbuffers
                          ${PTHREAD_LIBRARY})
```
with:
```
target_link_libraries(plasma_java
                          plasma_shared
                          ${PLASMA_LINK_LIBS}
                          "-undefined dynamic_lookup"
                          ${PTHREAD_LIBRARY})

```
- line 200 replace
```
target_link_libraries(plasma_java
                          plasma_shared
                          ${PLASMA_LINK_LIBS}
                          -Wl,--whole-archive
                          flatbuffers::flatbuffers
                          -Wl,--no-whole-archive
                          flatbuffers::flatbuffers
                          ${PTHREAD_LIBRARY})

```
with:
```
target_link_libraries(plasma_java
                          plasma_shared
                          ${PLASMA_LINK_LIBS}
                          ${PTHREAD_LIBRARY})

```
Then we need to change the installation script to `$CONDA_PREFIX/tmp/arrow/java/plasma/test.sh` by replacing the last two lines:
```
export PLASMA_STORE=$ROOT_DIR/../../cpp/release/release/plasma_store_server
java -cp target/test-classes:target/classes -Djava.library.path=$ROOT_DIR/../../cpp/release/release/ org.apache.arrow$
```
with:
```
export PLASMA_STORE=$CONDA_PREFIX/tmp/arrow/cpp/release/release/plasma-store-server
java -cp target/test-classes:target/classes -Djava.library.path=$CONDA_PREFIX/tmp/arrow/cpp/release/release/ org.apache.arrow.plasma.PlasmaClientTest
```
> If running into issues, add `sudo` call before the `cmake` and `make` commands