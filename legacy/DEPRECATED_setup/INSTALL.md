# Install

This file's goal is to document the installation script in this setup folder
to make it easier to understand what happens in there. Since the installation
of the `plasma_java` lib was very tricky, we ended up needing all of this.

## Cloning repo
First we activate the conda environment:
``` 
conda activate conda-arrow
```
Now we will clone our custom fork of the apache-arrow repo into our env:
```
cd $CONDA_PREFIX
mkdir tmp && cd tmp
git clone git@github.com:piaverous/arrow.git
```

## Installing everything
Now, the tricky part.
We first have to install some libs:
```
conda config --add channels conda-forge
conda update --all -q -y
conda install -c conda-forge -y \
        --file arrow/ci/conda_env_cpp.yml \
        --file arrow/ci/conda_env_gandiva.yml \
        --file arrow/ci/conda_env_unix.yml
conda clean --all -y
```
> NOTE: if you run into an error it's probably because you do not have the permissions to execute conda. You cannot run 
> it as `root` user so you have to upgrade your own privileges:
> `sudo chmod -R +x /home/<user>/miniconda3`

You can now install the lib and compile plasma_java:
```
cd $CONDA_PREFIX/tmp/arrow/java
mvn clean install -pl plasma -am -Dmaven.test.skip
cd $CONDA_PREFIX/tmp/arrow/java/plasma
./test.sh
```
If the tests are passing, you're all good!


 