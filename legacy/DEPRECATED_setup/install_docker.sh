#!/bin/bash

echo "------------- CLONING FORK REPO -------------"
echo ""
git clone https://www.github.com/apache/arrow.git /arrow
pushd /arrow || exit
    echo "---------------------------------------------"
    echo "------------- INSTALL LIBRARIES -------------"
    echo ""
    conda config --add channels conda-forge
    conda install -c conda-forge -y --file ci/conda_env_cpp.yml --file ci/conda_env_gandiva.yml --file ci/conda_env_unix.yml
    conda clean --all -y
    echo "---------------------------------------------"
    echo "---------- INSTALL PLASMA BINARIES ----------"
    echo ""
    pushd java || exit
        mvn clean install -pl plasma -am -Dmaven.test.skip
        echo "---------------------------------------------"
        echo "------------ TEST PLASMA INSTALL ------------"
        echo ""
        pushd plasma || exit
            ./test.sh
        popd
    popd
popd


echo "------------- DONE ! ALL GOOD :) ------------"
