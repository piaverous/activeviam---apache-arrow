#!/bin/bash
source ~/.bash_profile

conda activate conda-arrow

pushd $CONDA_PREFIX || exit
    sudo rm -rf tmp
    mkdir tmp
    pushd tmp || exit
        echo "------------- CLONING FORK REPO -------------"
        echo ""
        git clone git@github.com:piaverous/arrow.git
        pushd arrow || exit
            echo "---------------------------------------------"
            echo "------------- INSTALL LIBRARIES -------------"
            echo ""
            conda config --add channels conda-forge
            conda install -c conda-forge -y --file ci/conda_env_cpp.yml
            conda clean --all -y
            echo "---------------------------------------------"
            echo "---------- INSTALL PLASMA BINARIES ----------"
            echo ""
            pushd java || exit
                mvn clean install -pl plasma -am -Dmaven.test.skip
                echo "---------------------------------------------"
                echo "------------ TEST PLASMA INSTALL ------------"
                echo ""
                pushd plasma || exit
                    ./test.sh
                popd || exit
            popd || exit
        popd || exit
    popd || exit
popd || exit

echo "------------- DONE ! ALL GOOD :) ------------"
conda deactivate