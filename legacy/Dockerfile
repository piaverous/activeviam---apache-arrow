ARG arch=amd64
ARG jdk=8
ARG maven=3.5.4
FROM ${arch}/maven:${maven}-jdk-${jdk}
ENV ARROW_JAVA_SHADE_FLATBUFS=ON

# install build essentials
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update -y -q && \
    apt-get install -y -q --no-install-recommends \
    ca-certificates \
    ccache \
    clang-7 \
    cmake \
    git \
    g++ \
    gcc \
    libboost-all-dev \
    libgflags-dev \
    libgoogle-glog-dev \
    libgtest-dev \
    liblz4-dev \
    libre2-dev \
    libsnappy-dev \
    libssl-dev \
    llvm-7-dev \
    make \
    ninja-build \
    pkg-config \
    protobuf-compiler \
    rapidjson-dev \
    thrift-compiler \
    tzdata \
    zlib1g-dev \
    wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ARG cmake=3.11.4
RUN wget -nv -O - https://github.com/Kitware/CMake/releases/download/v${cmake}/cmake-${cmake}-Linux-x86_64.tar.gz | tar -xzf - -C /opt
ENV PATH=/opt/cmake-${cmake}-Linux-x86_64/bin:$PATH

ENV ARROW_BUILD_TESTS=OFF \
    ARROW_FLIGHT=OFF \
    ARROW_GANDIVA_JAVA=ON \
    ARROW_GANDIVA=ON \
    ARROW_HOME=/usr/local \
    ARROW_JNI=ON \
    ARROW_ORC=ON \
    ARROW_PARQUET=OFF \
    ARROW_PLASMA_JAVA_CLIENT=ON \
    ARROW_PLASMA=ON \
    ARROW_USE_CCACHE=ON \
    CC=gcc \
    CXX=g++ \
    ORC_SOURCE=BUNDLED \
    PATH=/usr/lib/ccache/:$PATH \
    Protobuf_SOURCE=BUNDLED

RUN git clone https://github.com/apache/arrow /arrow \
    && /bin/bash -c " \
    /arrow/ci/scripts/cpp_build.sh /arrow /build && \
    /arrow/ci/scripts/java_build.sh /arrow /build"

ENV PLASMA_STORE=/usr/local/bin/plasma-store-server \
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH/build/cpp/${ARROW_BUILD_TYPE:-debug}

CMD ["/bin/bash"]
