# [ActiveViam] - Projet d'option ISIA

In this project, we basically want to be able to run a plasma_store
instance in one process, a Python process that can read/write into it and a Java process that
can read it. We will then play around with all of that.

## Run with Docker

The project environment is easy to setup with docker.
A container sharing the `./src` directory containing the source files is used to run everything.

### Pulling the image

It is recommended to pull the image by doing the following:

```
docker pull donacrio/arrow
```

### Building the image

Alternatively you can build the docker image by running:

> ⚠️ Building the image can take a while, make sure you have
> a couple minutes, a good internet connection and enought free RAM.

```
docker build -t arrow .
```

Then run the image in a container and share the root of the project in a volume. You should run it interactively so you can interact:

```
docker run -it --name arrow_container -v /path/to/the/project/src:/src <image-name>
```

Now you can package the java source file by running:

```
cd /src/java-active-arrow
mvn clean package
```

You can also launch an instance of the plasma store:

```
/usr/local/bin/plasma-store-server -m 1000000 -s /tmp/plasma
```

## [DEPRECTED] Run locally

Because the installation required some manual tricks because of the lack of
automation for the `plasma_java` install (Java driver for the plasma store),
we will be using a miniconda environment to handle all our binaries and env
variables.

### Create miniconda environment

We will use miniconda 3 to create a running environment for the plasma store.
You can download the latest release of miniconda 3 on the official website:
https://docs.conda.io/en/latest/miniconda.html. Then, just follow the steps.

> Note: you do not have to use miniconda3 here, you can use anaconda3 as well.
> miniconda3 is just a bit lighter :)

Lets create a new conda environment named `conda-arrow` running with Python3.6:

```
conda create -n conda-arrow python=3.6
```

Then jump into it:

```
conda activate conda-arrow
```

### Install pyarrow and the plasma store

Pyarrow is the Python API for arrow (thus the plasma store).
We install arrow and pyarrow from the `conda-forge` repository:

```
conda install -c conda-forge -y --file conda.yml
```

This will also install the binaries for the plasma store.

> If the root privileges are needed for conda execution you
> can elevate the privileges using:
> `sudo chmod -R +x /home/<user>/miniconda3`

### Install plasma_java

> ⚠️ This install can take a while, make sure you have
> a couple minutes, a good internet connection and enought free RAM.

#### macOS

This should be as easy as running:

```
./DEPRECATED_setup/install_mac.sh
```

#### Ubuntu

By default conda initialization is made into `~/.bashrc`
but in non-interactive shell this file is not sourced.
Hence you need to run the `.sh` script with the `bash` command:

```
bash ./DEPRECATED_setup/install_unix.sh
```

## Getting started

### Python

A testing Notebook is available to play around ! To use it, start the plasma_store
in a separate terminal window by doing the following:

```
conda activate conda-arrow
plasma_store -m 1000000000 -s /tmp/plasma
```

Then run the notebook like so:

```
cd py-active-arrow
jupyter notebook Testing\ Notebook.ipynb
```

### Java

A tiny bit trickier, but not as much since we clarified everything !
Like for Python, you'll need the plasma_store running in a separate terminal:

```
conda activate conda-arrow
plasma_store -m 1000000000 -s /tmp/plasma
```

You can then write your java code, and to run it do the following:

```
cd java-active-arrow
mvn package
bash ./scripts/run.sh
```

There you go, you're all set !

## Authors

- **Thibault de Rubercy** - _Expert Python_ - https://www.youtube.com/watch?v=8lVqEchxIxw
- **Donatien Criaud** - _Expert Java_ - https://www.youtube.com/watch?v=9E7tEG_a9Ws
- **Pierre Averous** - _Expert Rust_ - https://www.youtube.com/watch?v=Uj1buI29lNg
