#!/bin/bash

export PLASMA_STORE=/arrow/cpp/release/release/plasma-store-server

mvn clean package -X -Djava.library.path=/arrow/cpp/release/release -Dmaven.test.skip=true