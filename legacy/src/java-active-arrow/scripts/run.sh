#!/bin/bash
source ~/.bash_profile

export PLASMA_STORE=$CONDA_PREFIX/tmp/arrow/cpp/release/release/plasma-store-server

java -cp $CONDA_PREFIX/tmp/arrow/java/plasma/target/test-classes:$CONDA_PREFIX/tmp/arrow/java/plasma/target/classes \
  -Djava.library.path=$CONDA_PREFIX/tmp/arrow/cpp/release/release \
  -jar ./target/arrow-1.0-SNAPSHOT-jar-with-dependencies.jar
