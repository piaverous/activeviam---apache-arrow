#!/bin/bash

export PLASMA_STORE=/arrow/cpp/release/release/plasma-store-server

java -cp /arrow/java/plasma/target/test-classes:/arrow/java/plasma/target/classes \
  -Djava.library.path=/arrow/cpp/release/release \
  -jar ./target/arrow-1.0-SNAPSHOT-jar-with-dependencies.jar