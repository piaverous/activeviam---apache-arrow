package com.activeviam.arrow;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVEvaluator {

    public List<List<String>> read_and_write_csv() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader("./data/test_data.csv"));
        List<List<String>> rows = new ArrayList<>();

        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            // do something with the data
            rows.add(Arrays.asList(data));
        }
        csvReader.close();

        FileWriter csvWriter = new FileWriter("./data/tmp.csv");
        for (List<String> rowData : rows) {
            csvWriter.append(String.join(",", rowData));
            csvWriter.append("\n");
        }
        csvWriter.flush();
        csvWriter.close();

        return rows;
    }

    public double measure(int nbIterations) throws IOException {
        List<Long> iterTimes = new ArrayList<>();
        for (int i = 0; i < nbIterations; i++) {
            long startTime = System.nanoTime();
            read_and_write_csv();
            long endTime = System.nanoTime();
            System.out.print(".");
            long duration = (endTime - startTime)/1000000;  //divide by 1000000 to get milliseconds.
            iterTimes.add(duration);
        }
        System.out.print("\n");
        return iterTimes.stream().mapToLong(l -> l).average().orElse(0);
    }
}
