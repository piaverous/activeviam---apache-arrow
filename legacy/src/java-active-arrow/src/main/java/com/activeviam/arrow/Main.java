package com.activeviam.arrow;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        int nbIterations = 10;
        CSVEvaluator csvEvaluator = new CSVEvaluator();
        System.out.println("Starting csv method measuring...");
        double csv_duration = csvEvaluator.measure(nbIterations);
        System.out.format("Execution took %s ms per step\n", csv_duration);

        List<List<String>> rows = csvEvaluator.read_and_write_csv();
        PlasmaEvaluator plasmaEvaluator = new PlasmaEvaluator();
        double plasma_duration = plasmaEvaluator.measure(nbIterations, rows);
        System.out.format("Execution took %s ms per step\n", plasma_duration);
    }
}

