package com.activeviam.arrow;

import org.apache.arrow.plasma.ObjectStoreLink;
import org.apache.arrow.plasma.PlasmaClient;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlasmaEvaluator {
    private ObjectStoreLink plasmaStoreLink;

    public PlasmaEvaluator() {
        System.loadLibrary("plasma_java");
        this.plasmaStoreLink = new PlasmaClient("/tmp/plasma", "", 0);
    }

    public byte[] strings_to_bytes(List<String> list) throws IOException {
        // write to byte array
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(baos);
        for (String element : list) {
            out.writeUTF(element);
        }
        return baos.toByteArray();
    }

    public List<String> bytes_to_strings(byte[] bytes) throws IOException {
        // read from byte array
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        DataInputStream in = new DataInputStream(bais);
        List<String> strings = new ArrayList<>();
        while (in.available() > 0) {
            strings.add(in.readUTF());
        }
        return strings;
    }

    public void write_and_read_plasma(List<List<String>> rows) throws IOException {
        int timeoutMs = 3000;
        for (int i=0; i<rows.size(); i++) {
            byte[] id = new byte[20];
            Arrays.fill(id, (byte)i);

            byte[] value = strings_to_bytes(rows.get(i));
            plasmaStoreLink.delete(id);
            plasmaStoreLink.put(id, value, null);
        }
        // Wrote all in plasma store, now let's retrieve it
        for (int i=0; i<rows.size(); i++) {
            byte[] id = new byte[20];
            Arrays.fill(id, (byte)i);

            this.plasmaStoreLink.get(id, timeoutMs, false);
            // Release object at ID, so next iteration does not fail
            this.plasmaStoreLink.release(id);
        }
    }

    public void cleanUp(int number) {
        for (int i=0; i<number; i++) {
            byte[] id = new byte[20];
            Arrays.fill(id, (byte)i);
            plasmaStoreLink.delete(id);
        }
    }

    public double measure(int nbIterations, List<List<String>> rows) throws IOException {
        System.out.println("Cleaning up the plasma store...");
        this.cleanUp(rows.size());
        System.out.println("Measuring plasma store performance...");

        List<Long> iterTimes = new ArrayList<>();
        for (int i = 0; i < nbIterations; i++) {
            long startTime = System.nanoTime();
            this.write_and_read_plasma(rows);
            long endTime = System.nanoTime();
            System.out.print(".");
            long duration = (endTime - startTime)/1000000;  //divide by 1000000 to get milliseconds.
            iterTimes.add(duration);
        }
        System.out.print("\n");
        return iterTimes.stream().mapToLong(l -> l).average().orElse(0);
    }
}
