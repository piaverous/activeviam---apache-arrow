package com.activeviam.arrow;

import com.activeviam.arrow.controllers.PlasmaController;
import com.activeviam.arrow.exceptions.EnvVariableMalformedException;
import com.activeviam.arrow.exceptions.EnvVariableNotSetException;
import com.activeviam.arrow.exceptions.MalformedObjectIdException;
import com.activeviam.arrow.exceptions.SystemEnvAccessNotAllowedException;
import com.activeviam.arrow.exceptions.UnhandledMinorTypeException;
import com.activeviam.arrow.io.ReadFromArrowStream;
import com.activeviam.arrow.utils.ByteUtils;
import com.activeviam.arrow.utils.EnvUtils;
import com.activeviam.arrow.utils.Timer;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import org.apache.arrow.vector.ipc.ArrowReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App implements Runnable {
  private static final Logger logger = LoggerFactory.getLogger(App.class);

  private final String plasmaSocketPath;
  private final int serverSocketPort;
  private final Timer timer;
  private final int maxLoops;
  private int numberOfLoops;
  private boolean isRunning;

  public App()
      throws EnvVariableNotSetException, SystemEnvAccessNotAllowedException,
          EnvVariableMalformedException, IOException {
    this.plasmaSocketPath = EnvUtils.getEnvVariableAsString("PLASMA_SOCKET_PATH");
    this.serverSocketPort = EnvUtils.getEnvVariableAsInteger("SERVER_SOCKET_PORT");
    this.timer = new Timer(true);
    this.maxLoops = EnvUtils.getEnvVariableAsInteger("LOOPS");
    this.isRunning = false;
    this.numberOfLoops = 1;
  }

  @Override
  public void run() {
    logger.info("Starting the Java process");
    this.isRunning = true;
    this.timer.start();
    PlasmaController plasmaController = new PlasmaController(this.plasmaSocketPath);
    ServerSocket serverSocket = null;
    Socket clientSocket = null;
    InputStream inputStream = null;
    BufferedWriter bufOutputWriter;
    ReadFromArrowStream readFromArrowStream;

    try {
      readFromArrowStream = new ReadFromArrowStream(this.timer);
      serverSocket = new ServerSocket(this.serverSocketPort);

      while (this.numberOfLoops <= this.maxLoops && this.isRunning) {
        int numberOfRows = (int) Math.pow(10, this.numberOfLoops);
        this.timer.debug("Awaiting client connection");
        logger.debug("Socket server listening on {port={}}", this.serverSocketPort);
        clientSocket = serverSocket.accept();
        this.timer.info("Client connected to socket");

        // We use a BufferedInputStream to increase reading performances
        inputStream = new BufferedInputStream(clientSocket.getInputStream());
        bufOutputWriter =
            new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        bufOutputWriter.write("STA");
        bufOutputWriter.flush();
        this.timer.debug("Sent 'STA' (start) signal to Python client");

        byte[] objectId = ByteUtils.readInputStream(inputStream);
        this.timer.debug("Received an ObjectId");

        try {
          ArrowReader dataStream =
              plasmaController.getDataStreamReader(ByteUtils.ensureObjectId(objectId));
          readFromArrowStream.read(dataStream);
          this.timer.info(String.format("Done reading the Arrow Table, %d rows", numberOfRows));
        } catch (UnhandledMinorTypeException | MalformedObjectIdException e) {
          this.timer.info("An error occurred while reading the Arrow Table");
          e.printStackTrace();
        } finally {
          this.numberOfLoops++;
        }
      }

    } catch (IOException ex) {
      logger.error("An error occurred", ex);
    } finally {
      this.timer.stop();
      try {
        logger.info("Closing streams and sockets");
        this.isRunning = false;
        if (!Objects.isNull(inputStream)) {
          inputStream.close();
        }
        if (!Objects.isNull(clientSocket)) {
          clientSocket.close();
        }
        if (!Objects.isNull(serverSocket)) {
          serverSocket.close();
        }
      } catch (IOException ex) {
        logger.error("An error occurred", ex);
      }
      this.stop();
    }
    logger.info("Java process terminated");
  }

  public void stop() {
    this.isRunning = false;
  }
}
