package com.activeviam.arrow.controllers;

import com.activeviam.arrow.exceptions.MalformedObjectIdException;
import com.activeviam.arrow.utils.ByteUtils;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.plasma.ObjectStoreLink;
import org.apache.arrow.plasma.PlasmaClient;
import org.apache.arrow.vector.ipc.ArrowStreamReader;

public class PlasmaController {
  private final ObjectStoreLink plasmaStoreLink;
  private final int timeoutMs;

  public PlasmaController(String storeSocketName) {
    this(storeSocketName, 1000);
  }

  public PlasmaController(String storeSocketName, int timeoutMs) {
    System.loadLibrary("plasma_java");
    this.plasmaStoreLink = new PlasmaClient(storeSocketName, "", 0);
    this.timeoutMs = timeoutMs;
  }

  public InputStream getDataAsBytes(byte[] id) throws MalformedObjectIdException {
    return new ByteArrayInputStream(
        this.plasmaStoreLink.get(ByteUtils.ensureObjectId(id), this.timeoutMs, false));
  }

  public ArrowStreamReader getDataStreamReader(byte[] id) throws MalformedObjectIdException {
    return new ArrowStreamReader(this.getDataAsBytes(id), new RootAllocator());
  }
}
