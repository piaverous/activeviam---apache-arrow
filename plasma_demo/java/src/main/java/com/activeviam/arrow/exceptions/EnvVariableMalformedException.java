package com.activeviam.arrow.exceptions;

public class EnvVariableMalformedException extends Throwable {
  public EnvVariableMalformedException(String envVariableName, String errorMessage) {
    super(String.format("Malformed environment variable %s: %s", envVariableName, errorMessage));
  }
}
