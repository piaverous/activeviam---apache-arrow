package com.activeviam.arrow.exceptions;

public class MalformedObjectIdException extends Throwable {
  public MalformedObjectIdException(byte[] id) {
    super(String.format("Object id is malformed {objectId=%s}", new String(id)));
  }
}
