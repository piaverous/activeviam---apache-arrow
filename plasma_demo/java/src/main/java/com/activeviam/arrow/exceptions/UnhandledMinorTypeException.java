package com.activeviam.arrow.exceptions;

import org.apache.arrow.vector.types.Types;

public class UnhandledMinorTypeException extends Throwable {
  public UnhandledMinorTypeException(Types.MinorType minorType) {
    super(String.format("Unhandled Apache Arrow minor type: %s", minorType.toString()));
  }
}
