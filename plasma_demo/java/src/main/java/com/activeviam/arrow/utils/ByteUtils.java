package com.activeviam.arrow.utils;

import com.activeviam.arrow.exceptions.MalformedObjectIdException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteUtils {
  public static byte[] ensureObjectId(byte[] id) throws MalformedObjectIdException {
    if (id.length != 20) {
      throw new MalformedObjectIdException(id);
    }
    return id;
  }

  public static byte[] readInputStream(InputStream is) throws IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    // 4096 is the minimum number of bytes read by BufferedInputStream so it's useless to have a
    // shorter buffer.
    byte[] data = new byte[4096];
    int next;
    while ((next = is.read(data, 0, data.length)) != -1) {
      buffer.write(data, 0, next);
    }
    buffer.flush();
    return buffer.toByteArray();
  }
}
