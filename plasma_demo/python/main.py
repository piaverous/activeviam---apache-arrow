#!/usr/bin/env python3

import socket

import pyarrow as pa
import pyarrow.plasma as plasma

from config import PLASMA_SOCKET_PATH, SERVER_SOCKET_HOST, SERVER_SOCKET_PORT, LOG_LEVEL, GLOBAL_TIMEOUT, LOOPS
from utils.data_gen import gen_dataframe
from utils.docker_logger import DockerLogger
from utils.env_utils import get_log_level_as_int
from utils.io import expect_start_signal
from utils.looper import Looper
from utils.plasma import write_to_plasma
from utils.stopwatch import StopWatch
from utils.string_utils import human_readable_size

LOGGER = DockerLogger("main", log_level=get_log_level_as_int(LOG_LEVEL))
DEMO_LOGGER = DockerLogger("demo", log_level=get_log_level_as_int(LOG_LEVEL))
STOP_WATCH = StopWatch(log_level=0)


def run_the_demo(plasma_client, n_rows=1e6):
    DEMO_LOGGER.info(f"Started demo with n_rows={n_rows}")
    STOP_WATCH.lap(f"Started demo with n_rows={n_rows}")

    DEMO_LOGGER.debug("Generating dataframe")
    df = gen_dataframe(n_rows)
    df_mem_size_readable = human_readable_size(df.memory_usage(index=True).sum())
    STOP_WATCH.lap(f"Generated dataframe, rows: {n_rows}, size: {df_mem_size_readable}")

    DEMO_LOGGER.debug("Converting dataframe to Arrow Table")
    record_batch = pa.RecordBatch.from_pandas(df)
    STOP_WATCH.lap("Converted dataframe to Arrow Table")

    DEMO_LOGGER.debug("Connecting to java server socket")
    client_socket = socket.socket()
    client_socket.connect((SERVER_SOCKET_HOST, int(SERVER_SOCKET_PORT)))
    STOP_WATCH.lap("Connected to java server socket")

    DEMO_LOGGER.debug("Waiting for 'STA' signal")
    start_signal = expect_start_signal(client_socket)
    STOP_WATCH.lap("Got 'STA' signal")

    DEMO_LOGGER.debug("Writing object to plasma store")
    object_id = write_to_plasma(record_batch, plasma_client)
    STOP_WATCH.lap("Wrote object to plasma store")

    DEMO_LOGGER.debug("Sending ObjectId to Java process")
    bytes_id = object_id.binary()
    client_socket.send(bytes_id)
    STOP_WATCH.lap("Sent object_id to Java process")

    DEMO_LOGGER.debug("Closing socket connection")
    client_socket.close()
    del df
    del record_batch
    STOP_WATCH.lap("Closed socket connection")

    return start_signal == b"STA"


if __name__ == "__main__":
    LOGGER.info("Starting the Python process")
    n_iterations = int(LOOPS)

    LOGGER.info("Opening plasma store connection")
    client = plasma.connect(PLASMA_SOCKET_PATH)
    LOGGER.info("Connected to plasma store")

    LOGGER.info(f"Launching a looper to run the demo {n_iterations} times")
    STOP_WATCH.start()
    looper = Looper(run_the_demo, client, get_log_level_as_int(LOG_LEVEL), loop_limit=n_iterations)
    looper.start()
    LOGGER.info("Looper started")

    looper.join(GLOBAL_TIMEOUT)
    if looper.is_alive():
        looper.stop()
    STOP_WATCH.stop()

    client.disconnect()
    LOGGER.info("Python process terminated")
