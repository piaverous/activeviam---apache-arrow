def expect_start_signal(socket):
    start_signal = socket.recv(8)
    if start_signal != b"STA":
        raise Exception("Signal does not correspond to start signal")
    return start_signal
