import threading
import time

from utils.docker_logger import DockerLogger


class Looper(threading.Thread):
    def __init__(self, func_to_run, plasma_client, log_level, loop_limit=5):
        threading.Thread.__init__(self)
        self.logger = DockerLogger("looper", log_level=log_level)
        self.running = False
        self.no_fail = True
        self.loop_count = 0
        self.loop_limit = loop_limit

        self.func_to_run = func_to_run
        self.plasma_client = plasma_client

    def run(self):
        self.running = True
        while self.loop_count < self.loop_limit and self.running and self.no_fail:
            self.loop_count += 1
            self.no_fail = self.func_to_run(self.plasma_client, n_rows=10 ** self.loop_count)
            self.logger.info(f"Loop ran {self.loop_count} times")
            time.sleep(1)

        self.logger.info(f"Stopped running after {self.loop_count} out of {self.loop_limit} iterations.")
        if not self.no_fail:
            self.logger.err("Stopped because of a failure it seems")
        elif not self.running:
            self.logger.warn("Stopping after external trigger")

    def stop(self):
        self.running = False
        self.join()
