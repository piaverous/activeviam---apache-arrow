import numpy as np
import pyarrow as pa
import pyarrow.plasma as plasma


def generate_object_id():
    return plasma.ObjectID(np.random.bytes(20))


def get_record_batch_size(record_batch):
    mock_sink = pa.MockOutputStream()
    mock_stream_writer = pa.RecordBatchFileWriter(
        mock_sink, record_batch.schema)
    mock_stream_writer.write_batch(record_batch)
    mock_stream_writer.close()
    return mock_sink.size()


def write_to_plasma(record_batch, plasma_client):
    object_id = generate_object_id()
    size = get_record_batch_size(record_batch)

    buffer = plasma_client.create(object_id, size)
    stream = pa.FixedSizeBufferWriter(buffer)
    stream_writer = pa.RecordBatchStreamWriter(stream, record_batch.schema)
    stream_writer.write_batch(record_batch)
    stream_writer.close()

    plasma_client.seal(object_id)
    return object_id


def read_from_plasma(object_id, plasma_client):
    # Get PlasmaBuffer from ObjectID
    [data] = plasma_client.get_buffers([object_id])
    buffer = pa.BufferReader(data)
    reader = pa.RecordBatchStreamReader(buffer)
    return reader.read_next_batch()
