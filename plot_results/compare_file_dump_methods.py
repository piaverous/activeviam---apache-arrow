import matplotlib.pyplot as plt
import numpy as np

from utils.extract import extract_logs_for_demo
from utils.size import human_readable_size
plt.rc('figure', figsize=(8, 6))


if __name__ == "__main__":
    _, csv_rt, csv_pt, _, csv_wt, csv_cvt, csv_f_size = extract_logs_for_demo("csv_demo")
    _, dump_rt, dump_pt, _, dump_wt, dump_cvt, dump_f_size = extract_logs_for_demo("dump_demo")
    _, direct_rt, direct_pt, _, direct_wt, direct_cvt, direct_f_size = extract_logs_for_demo("direct_demo")
    _, plasma_rt, plasma_pt, _, plasma_wt, plasma_cvt, plasma_f_size = extract_logs_for_demo("plasma_demo")
    x = np.arange(len(plasma_f_size))  # the label locations

    axis_ticks = [human_readable_size(size) for size in plasma_f_size]

    fig, ax = plt.subplots()

    width = 0.4
    r1 = ax.bar(x-width/2, csv_pt, width, label='.csv dump')
    r2 = ax.bar(x+width/2, dump_rt+dump_pt+dump_wt+dump_cvt, width, label='.arrow dump')

    ax.set_ylabel('Transfer duration (ms)')
    ax.set_title("Data transfer duration for .CSV and .arrow dump methods")
    ax.set_xticks(x)
    ax.set_xticklabels(axis_ticks)
    ax.legend()
    ax.set_xlabel("Size of transferred data")


    def label(rects):
        for rect in rects:
            height = rect.get_height()
            if height < 180000:
                ax.annotate('{:.0f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center',
                            va='bottom',
                            rotation=90)
            else:
                ax.annotate('{:.0f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, -40),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center',
                            va='bottom',
                            color="white",
                            rotation=90)

    label(r1)
    label(r2)

    plt.savefig("results/compare_file_dumps.png")
    plt.show()

    # Compare acceleration compared to csv method
    dump_gain = csv_pt/(dump_rt+dump_pt+dump_wt+dump_cvt)


    dump_gain = [gain if gain >= 1 else -1/gain for gain in dump_gain]

    width = 0.3  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, dump_gain, width, label='.arrow dump')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    plt.xlabel("Size of transferred data")
    ax.set_ylabel('Acceleration')
    ax.set_title("Acceleration compared to .CSV dump method")
    ax.set_xticks(x)
    ax.set_xticklabels(axis_ticks)
    ax.legend()


    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{:.0f}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')


    autolabel(rects1)

    fig.tight_layout()

    plt.savefig("results/accelerations_file_dump.png")
    plt.show()
