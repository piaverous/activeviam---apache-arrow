import matplotlib.pyplot as plt
import numpy as np

from utils.extract import extract_logs_for_demo
from utils.size import human_readable_size
plt.rc('figure', figsize=(8, 6))


if __name__ == "__main__":
    _, csv_rt, csv_pt, _, csv_wt, csv_cvt, csv_f_size = extract_logs_for_demo("csv_demo")
    _, dump_rt, dump_pt, _, dump_wt, dump_cvt, dump_f_size = extract_logs_for_demo("dump_demo")
    _, direct_rt, direct_pt, _, direct_wt, direct_cvt, direct_f_size = extract_logs_for_demo("direct_demo")
    _, plasma_rt, plasma_pt, _, plasma_wt, plasma_cvt, plasma_f_size = extract_logs_for_demo("plasma_demo")

    axis_ticks = [human_readable_size(size) for size in plasma_f_size]

    plt.plot(plasma_f_size, csv_pt)
    plt.plot(plasma_f_size, dump_rt+dump_wt)
    plt.plot(plasma_f_size, direct_rt+direct_wt)
    plt.plot(plasma_f_size, plasma_rt+plasma_wt)

    plt.xscale("log")
    plt.tick_params(which='x', width=0, length=0, color='w')

    plt.xticks(plasma_f_size, axis_ticks, weight=510)
    plt.legend([".csv dump", ".arrow dump", "socket transfer", "plasma store"])

    plt.xlabel("Size of transferred data")
    plt.ylabel("Transfer duration (ms)")
    plt.title("Comparison of all 4 data transfer methods")

    plt.savefig("results/compare_all_total.png")
    plt.show()

    # Compare acceleration compared to csv method
    dump_gain = csv_pt/(dump_rt+dump_pt+dump_wt+dump_cvt)
    direct_gain = csv_pt/(direct_rt+direct_pt+direct_wt+direct_cvt)
    plasma_gain = csv_pt/(plasma_rt+plasma_pt+plasma_wt+plasma_cvt)


    dump_gain = [gain if gain >= 1 else -1/gain for gain in dump_gain]
    direct_gain = [gain if gain >= 1 else -1/gain for gain in direct_gain]
    plasma_gain = [gain if gain >= 1 else -1/gain for gain in plasma_gain]

    x = np.arange(len(plasma_f_size))  # the label locations
    width = 0.3  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, dump_gain, width, label='.arrow dump')
    rects2 = ax.bar(x, direct_gain, width, label='socket transfer')
    rects3 = ax.bar(x + width, plasma_gain, width, label='plasma store')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    plt.xlabel("Size of transferred data")
    ax.set_ylabel('Acceleration')
    ax.set_title("Acceleration compared to .CSV dump method")
    ax.set_xticks(x)
    ax.set_xticklabels(axis_ticks)
    ax.legend()


    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{:.0f}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')


    autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)

    fig.tight_layout()

    plt.savefig("results/accelerations.png")
    plt.show()
