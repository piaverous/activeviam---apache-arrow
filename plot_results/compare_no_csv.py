import matplotlib.pyplot as plt

from utils.size import human_readable_size
from utils.extract import extract_logs_for_demo
plt.rc('figure', figsize=(8, 6))


if __name__ == "__main__":
    _, dump_rt, dump_pt, _, dump_wt, dump_cvt, dump_f_size = extract_logs_for_demo("dump_demo")
    _, direct_rt, direct_pt, _, direct_wt, direct_cvt, direct_f_size = extract_logs_for_demo("direct_demo")
    _, plasma_rt, plasma_pt, _, plasma_wt, plasma_cvt, plasma_f_size = extract_logs_for_demo("plasma_demo")

    axis_ticks = [human_readable_size(size) for size in plasma_f_size]

    plt.plot(plasma_f_size, dump_rt+dump_wt)
    plt.plot(plasma_f_size, direct_rt+direct_wt)
    plt.plot(plasma_f_size, plasma_rt+plasma_wt)

    plt.xscale("log")
    plt.tick_params(which='x', width=0, length=0, color='w')

    plt.xticks(plasma_f_size, axis_ticks, weight=510)
    plt.legend([".arrow dump", "socket transfer", "plasma store"])

    plt.xlabel("Size of transferred data")
    plt.ylabel("Transfer duration (ms)")
    plt.title("Comparison of all Apache Arrow based transfer methods ")

    plt.savefig("results/compare_no_csv.png")
    plt.show()

