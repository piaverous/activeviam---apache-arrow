import matplotlib.pyplot as plt
import numpy as np

from utils.extract import extract_logs_for_demo
from utils.size import human_readable_size

plt.rc('figure', figsize=(8, 6))


if __name__ == "__main__":
    java_n, java_frt, java_fpt, py_n, py_fwt, py_cvt, sizes = extract_logs_for_demo("csv_demo")

    naive_times = java_fpt + py_fwt
    smart_times = java_fpt
    axis_ticks = [human_readable_size(size) for size in sizes]
    x = np.arange(len(sizes))  # the label locations

    plt.plot(x, naive_times)
    plt.plot(x, smart_times)

    plt.xticks(x, axis_ticks, weight=510)
    plt.legend(["naive approach", "synchronized approach"])

    plt.xlabel("Size of transferred data")
    plt.ylabel("Transfer duration (ms)")
    plt.title("Duration of CSV method data transfer between processes")
    
    plt.savefig('results/csv_compare.png')
    plt.show()


    width = 0.35
    p1 = plt.bar(x, py_fwt, width)
    p2 = plt.bar(x, java_fpt, width, bottom=py_fwt)

    plt.ylabel("Transfer duration (ms)")
    plt.title('Duration of CSV method data transfer')

    plt.xticks(x, axis_ticks, weight=510)
    plt.legend(
        (p1[0], p2[0]),
        ('Data write time', 'Data read & parse time')
    )

    angle = 90
    min_display_height = 20000
    offset=3
    for (r1, r2) in zip(p1, p2):
        h1 = r1.get_height()
        h2 = r2.get_height()

        if h1 > min_display_height:
            plt.text(r1.get_x() + r1.get_width() / 2., h1 / 2., "%d" % h1, ha="center", va="center", color="white", rotation=angle)
        if h2 > min_display_height:
            plt.text(r2.get_x() + r2.get_width() / 2., h1 + h2 / 2., "%d" % h2, ha="center", va="center", color="white", rotation=angle)
        plt.text(r1.get_x() + r1.get_width() / 2, h1+h2+offset, "%d" % (h1+h2),ha='center', va='bottom')


    plt.savefig('results/csv_stack.png')
    plt.show()
