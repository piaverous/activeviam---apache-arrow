from utils.size import human_readable_size
from utils.extract import extract_logs_for_demo

import numpy as np
import matplotlib.pyplot as plt
plt.rc('figure', figsize=(8, 6))


if __name__ == "__main__":
    java_n, java_frt, java_fpt, py_n, py_fwt, py_cvt, sizes = extract_logs_for_demo("dump_demo")
    axis_ticks = [human_readable_size(size) for size in sizes]
    x = np.arange(len(sizes))  # the label locations

    plt.plot(x, java_frt)
    plt.plot(x, java_frt+java_fpt)
    plt.plot(x, py_fwt+py_cvt)

    plt.xticks(x, axis_ticks, weight=510)
    plt.legend(["File loading", "Total Read time", "Total Write time"])

    plt.xlabel("Size of transferred data")
    plt.ylabel("Transfer duration (ms)")
    plt.title("Duration of .arrow file dump data transfer between processes")

    plt.savefig('results/dump_compare.png')
    plt.show()

    width = 0.35

    p1 = plt.bar(x, py_cvt, width)
    p2 = plt.bar(x, py_fwt, width, bottom=py_cvt)
    p3 = plt.bar(x, java_frt, width, bottom=py_cvt+py_fwt)
    p4 = plt.bar(x, java_fpt, width, bottom=py_cvt+py_fwt+java_frt)

    plt.ylabel("Transfer duration (ms)")
    plt.title('Duration of data transfer for .arrow file dump')

    plt.xticks(x, axis_ticks, weight=510)
    plt.legend(
        (p1[0], p2[0], p3[0], p4[0]),
        ('Data conversion time', 'Data write time', 'Data read time', 'Data parse time')
    )

    min_display_height = 200
    angle = 90
    offset=3
    for (r1, r2, r3, r4) in zip(p1, p2, p3, p4):
        h1 = r1.get_height()
        h2 = r2.get_height()
        h3 = r3.get_height()
        h4 = r4.get_height()

        if h1 > min_display_height:
            plt.text(r1.get_x() + r1.get_width() / 2., h1 / 2., "%d" % h1, ha="center", va="center", color="white", rotation=angle)
        if h2 > min_display_height:
            plt.text(r2.get_x() + r2.get_width() / 2., h1 + h2 / 2., "%d" % h2, ha="center", va="center", color="white", rotation=angle)
        if h3 > min_display_height:
            plt.text(r3.get_x() + r3.get_width() / 2., h1 + h2 + h3 / 2., "%d" % h3, ha="center", va="center", color="white", rotation=angle)
        if h4 > min_display_height:
            plt.text(r4.get_x() + r4.get_width() / 2., h1 + h2 + h3 + h4 / 2., "%d" % h4, ha="center", va="center", color="white", rotation=angle)

        sum = h1 + h2 + h3 + h4
        plt.text(r1.get_x() + r1.get_width() / 2, sum + offset, "%d" % sum, ha='center', va='bottom')

    plt.savefig('results/dump_stack.png')
    plt.show()
