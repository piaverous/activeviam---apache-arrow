import matplotlib.pyplot as plt
import numpy as np

from utils.size import human_readable_size
from utils.extract import extract_logs_for_demo
plt.rc('figure', figsize=(8, 6))

if __name__ == "__main__":
    _, csv_rt, csv_pt, _, csv_wt, csv_cvt, csv_f_size = extract_logs_for_demo("csv_demo")
    _, dump_rt, dump_pt, _, dump_wt, dump_cvt, dump_f_size = extract_logs_for_demo("dump_demo")
    _, direct_rt, direct_pt, _, direct_wt, direct_cvt, direct_f_size = extract_logs_for_demo("direct_demo")
    _, plasma_rt, plasma_pt, _, plasma_wt, plasma_cvt, plasma_f_size = extract_logs_for_demo("plasma_demo")

    axis_ticks = [human_readable_size(size) for size in plasma_f_size]
    x = np.arange(len(plasma_f_size))  # the label locations

    # Compare acceleration compared to csv method
    width = 0.25  # the width of the bars
    spacing = width + 0.02

    fig, ax = plt.subplots()
    r1 = ax.bar(x - spacing, dump_rt, width, color='tab:orange')
    heights1 = [r.get_height() for r in r1]
    rr1 = ax.bar(x - spacing, dump_wt, width, bottom=dump_rt, color='tab:blue')
    heights1 = [heights1[i] + r.get_height() for i, r in enumerate(rr1)]
    rects1 = ax.bar(x - spacing, dump_pt, width, bottom=dump_rt+dump_wt, color='tab:green')

    r2 = ax.bar(x, direct_rt, width, color='tab:orange')
    heights2 = [r.get_height() for r in r2]
    rr2 = ax.bar(x, direct_wt, width, bottom=direct_rt, color='tab:blue')
    heights2 = [heights2[i] + r.get_height() for i, r in enumerate(rr2)]
    rects2 = ax.bar(x, direct_pt, width, bottom=direct_rt+direct_wt, color='tab:green')

    r3 = ax.bar(x + spacing, plasma_rt, width, color='tab:orange')
    heights3 = [r.get_height() for r in r3]
    rr3 = ax.bar(x + spacing, plasma_wt, width, bottom=plasma_rt, color='tab:blue')
    heights3 = [heights3[i] + r.get_height() for i, r in enumerate(rr3)]
    rects3 = ax.bar(x + spacing, plasma_pt, width, bottom=plasma_rt+plasma_wt, color='tab:green')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    plt.xlabel("Size of transferred data")
    ax.set_ylabel('Duration (ms)')
    ax.set_title("Comparison of 3 arrow-based methods \n Arrow file dump, direct socket transfer and plasma store transfer")
    ax.set_xticks(x)
    ax.set_xticklabels(axis_ticks)
    ax.legend()


    def autolabel(rects, heights):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for index, rect in enumerate(rects):
            height = rect.get_height() + heights[index]
            ax.annotate('{:.0f}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center',
                        va='bottom'
            )


    autolabel(rects1, heights1)
    autolabel(rects2, heights2)
    autolabel(rects3, heights3)
    
    plt.legend((r1[0], rr1[0], rects1[0], ), ('Read duration', 'Write duration', 'Read parsing duration'))

    plt.savefig("results/stacked_bars_arrow.png")
    plt.show()


    