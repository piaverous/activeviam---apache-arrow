from os import listdir
from os.path import isfile, join

import numpy as np
from utils.size import get_size_from_string


def extract_logs_for_demo(demo):
    """Extracts log data from a demo

    :param demo: string corresponding to demo name (i.e. direct_demo)
    :return: everything
    """
    dir_path = f"../{demo}/logs"
    files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
    java_file_read_times, java_file_process_time, java_n_measures = None, None, 0
    py_file_write_times, py_convert_times, py_n_measures = None, None, 0
    file_sizes = None

    for filename in files:
        with open(join(dir_path, filename), "r") as logfile:
            lines = [line.strip().split(" - ")[1:] for line in logfile.readlines()]
            if "JAVA" in filename:
                lines = [[l[0], int(l[1][:-2])] for l in lines]
                file_read_times = np.array([l[1] for l in lines if "Loaded" in l[0]])
                if java_file_read_times is None:
                    java_file_read_times = file_read_times
                else:
                    java_file_read_times += file_read_times

                processing_time = np.array([l[1] for l in lines if "Done" in l[0]])
                if java_file_process_time is None:
                    java_file_process_time = processing_time
                else:
                    java_file_process_time += processing_time
                java_n_measures += 1

            elif "PYTHON" in filename:
                lines = [[l[1], float(l[2][:-2])] for l in lines]
                file_write_times = np.array([
                    l[1] for l in lines if "Sent whole" in l[0] or "Wrote" in l[0]
                ])
                if py_file_write_times is None:
                    py_file_write_times = file_write_times
                else:
                    py_file_write_times += file_write_times

                convert_times = np.array([l[1] for l in lines if "Converted" in l[0]])
                if py_convert_times is None:
                    py_convert_times = convert_times
                else:
                    py_convert_times += convert_times
                py_n_measures += 1

                size_lines = np.array([l[0].split("size: ") for l in lines if "size:" in l[0]])
                str_sizes = [l[1] for l in size_lines]
                file_sizes = [get_size_from_string(size) for size in str_sizes]

    java_file_read_times = java_file_read_times / java_n_measures
    java_file_process_time = java_file_process_time / java_n_measures
    py_file_write_times = py_file_write_times / py_n_measures
    py_convert_times = py_convert_times / py_n_measures

    return java_n_measures, \
           java_file_read_times, \
           java_file_process_time, \
           py_n_measures, \
           py_file_write_times, \
           py_convert_times, \
           file_sizes