def get_size_from_string(size_string):
    for index, unit in enumerate(['KiB', 'MiB', 'GiB', 'TiB', 'B']):
        if unit in size_string:
            size = float(size_string[:len(size_string)-len(unit)])
            if unit != 'B':
                size = size * (1024**(index+1))
            break
    return size

def human_readable_size(size, decimal_places=1):
    for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB']:
        if size < 1024.0:
            break
        size /= 1024.0
    return f"{size:.{decimal_places}f}{unit}"