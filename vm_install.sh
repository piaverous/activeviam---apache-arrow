#!/bin/bash

echo "########## Starting setup ##########"

echo "### Installing fresh binaries ###"
sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y
sudo apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

echo "### Installing docker ###"
sudo apt remove -y docker docker-engine docker.io containerd runc
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io

echo "### Installing docker-compose ###"
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

echo "### Setting up group permissions ###"
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo service docker restart
sudo chmod +x /usr/local/bin/docker-compose

echo "########## Setup completed! ##########"
echo "You need to logout to activate new group priviledges"

exit